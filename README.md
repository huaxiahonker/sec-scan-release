## Sec-scan安全扫描系统   v0.1版本 ##

### 软件介绍： ### 源代码暂不开源
 **核心开发语言：** c# .netfraemwor4.7

 **默认数据库：** sqlite

 **数据库编辑：** SqliteStudio 3.3.3

 **支持数据库：** mysql、sql server

 **运行方式：** 安装运行、直接运行

 **支持系统：** windows系列

 **必要支持：** .net framework2 以上

 **程序官网：** https://sec-scan.srcbug.net  https://www.srcbug.net

系统核心功能：

1、漏洞扫描（基于POC的漏洞扫描，支持http\https协议）

2、子域名扫描（根据规则库扫描已知的子域名）

3、后台地址扫描（根据规则库扫描已知的网站后台地址）

4、端口扫描（支持全端口扫描、指定范围端口扫描，支持tcp\syn协议）

5、弱口令爆破（支持爆破多种服务及中间件；多种数据库、服务器）

6、指纹识别（根据指纹库可扫描如；CMS指纹、防火墙指纹、中间件等）

7、网络爬虫（爬取网站所有连接，并在一定程度上进行分析）

8、局域网扫描（支持如：无线密码破解、内网ARP攻击、远程信息探测及破解、端口扫描、共享扫描等）

9、C段扫描
## 启动检查 ##
启动时会检查各项组件是否存在，主要检查系统核心数据库是否存在


## 系统登录 ##
默认会调用系统机器码做为登录账号，首次使用不用进行登录


## 系统首页 ##
支持两大核心扫描（公网扫描、内网扫描）
## 软件下载 ##
https://gitee.com/huaxiahonker/sec-scan-release/archive/refs/tags/Sec-scan%E5%AE%89%E5%85%A8%E6%89%AB%E6%8F%8F%E7%B3%BB%E7%BB%9F%E5%8F%91%E5%B8%83%E7%89%88%E6%9C%AC.zip

https://gitee.com/huaxiahonker/sec-scan-release/archive/refs/tags/Sec-scan%E5%AE%89%E5%85%A8%E6%89%AB%E6%8F%8F%E7%B3%BB%E7%BB%9F%E5%8F%91%E5%B8%83%E7%89%88%E6%9C%AC.tar.gz

https://gitee.com/huaxiahonker/sec-scan-release/releases